﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Npgsql;

namespace PgManager
{
    internal class CommandsHelper<TEntity> where TEntity : class, new()
    {
        private const string SF = "select * from";
        private const string DF = "delete from";
        private const string W = "where";
        private const string O = "or";
        private const string S = "set";
        private const string U = "update";
        private const string I = "insert into";
        private const string V = "values";
        private const string R = "returning";

        private string Table => _entityType?.Name?.ToLower();
        private readonly Type _entityType;
        private readonly Dictionary<string, object> _dictionary;

        public CommandsHelper(object entity = null)
        {
            _dictionary = new Dictionary<string, object>();
            _entityType = typeof(TEntity);

            foreach (PropertyInfo property in _entityType.GetProperties())
            {
                if (entity is TEntity instance)
                {
                    _dictionary.Add(property.Name.ToLower(), property.GetValue(instance));
                    continue;
                }

                _dictionary.Add(property.Name.ToLower(), null);
            }
        }

        internal (string, IEnumerable<object>) InsertData()
        {
            var curr = _dictionary.Where(p => p.Key != "id" && p.Value != null);
            var props = curr.Select(p => p.Key);
            var signs = Enumerable.Range(1, curr.Count()).Select(x => $"${x}");

            return ($"{I} {Table} " +
                   $"({string.Join(", ", props)}) " +
                   $"{V} ({string.Join(", ", signs)}) " +
                   $"{R} id;", curr.Select(p => p.Value));
        }
        internal (string, IEnumerable<object>) UpdateData()
        {
            var curr = _dictionary.Where(p => p.Key != "id" && p.Value != null);
            var props = curr.Select(p => p.Key).ToArray();
            var signs = Enumerable.Range(1, props.Length).Select(x => $"{props[x - 1]}=${x}");
            var id = _dictionary.Where(p => p.Key == "id").Select(p => p.Value).Single();

            return ($"{U} {Table} " +
                   $"{S} {string.Join(", ", signs)} " +
                   $"{W} id={id}", curr.Select(p => p.Value));
        }
        internal string DeleteData(int entiryId)
        {
            return $"{DF} {Table} {W} id={entiryId}";
        }
        internal string MultipleDeleteData(int[] ids)
        {
            var query = ids.Select(x => $"id={x}");

            return $"{DF} {Table} {W} {string.Join($" {O} ", query)}";
        }
        internal string ClearData()
        {
            return $"{DF} {Table}";
        }
        internal string SelectDataById(int entityId)
        {
            return $"{SF} {Table} {W} id={entityId}";
        }
        internal string SelectData()
        {
            return $"{SF} {Table}";
        }
        internal TEntity CastData(DataTable table, int rowIndex = 0)
        {
            TEntity model = null;

            if (table != null && table.Rows != null && table.Rows.Count > rowIndex && table.Columns != null)
            {
                model = new TEntity();
                Dictionary<string, object> dict = new Dictionary<string, object>();
                DataRow row = table.Rows[rowIndex];
                DataColumnCollection columns = table.Columns;

                foreach (DataColumn column in columns)
                {
                    dict.Add(column.ColumnName, row.Field<object>(column));
                }

                foreach (var property in _entityType.GetProperties())
                {
                    try
                    {
                        if (dict.Keys.Contains(property.Name.ToLower()))
                        {
                            property.SetValue(model, dict[property.Name.ToLower()]);
                        }
                    }
                    catch { }
                }
            }

            return model;
        }
    }
    public interface ILogger
    {
        void Log(string message);
    }
    public class ConnectionString
    {
        private const string Result = "Server={0};Port={1};Database={2};User Id={3};Password={4};Timeout={5};";

        public string Server { get; set; }
        public int Port { get; set; }
        public string Database { get; set; }
        public string User { get; set; }
        public string Password { get; set; }
        public int Timeout { get; set; }

        public override string ToString() => string.Format(Result, Server, Port, Database, User, Password, Timeout);
    }
    public class Manager
    {
        private readonly ILogger _exceptionProvider;
        private ConnectionString _connectionString;
        private NpgsqlConnection _connection;
        private bool _commandExecuting;

        public Manager(ConnectionString connectionString, ILogger exceptionProvider)
        {
            _exceptionProvider = exceptionProvider;
            _connectionString = connectionString;
            _commandExecuting = false;
        }
        public Manager(ConnectionString connectionString)
        {
            _connectionString = connectionString;
            _commandExecuting = false;
        }

        public async Task ConnectAsync()
        {
            if (_commandExecuting)
            {
                return;
            }

            try
            {
                _commandExecuting = true;

                if (_connection != null && _connection.FullState == ConnectionState.Open)
                {
                    return;
                }

                _connection = new NpgsqlConnection(_connectionString.ToString());
                await _connection.OpenAsync();
            }
            catch (Exception ex)
            {
                _exceptionProvider?.Log(ex.Message);
            }
            finally
            {
                _commandExecuting = false;
            }
        }
        public async Task DisconnectAsync()
        {
            if (_commandExecuting)
            {
                return;
            }

            try
            {
                _commandExecuting = true;

                if (_connection == null)
                {
                    return;
                }

                await _connection.CloseAsync();
                await _connection.DisposeAsync();
            }
            catch (Exception ex)
            {
                _exceptionProvider?.Log(ex.Message);
            }
            finally
            {
                _commandExecuting = false;
            }
        }
        public async Task ReconnectAsync(ConnectionString connectionString)
        {
            if (_commandExecuting)
            {
                return;
            }

            try
            {
                _commandExecuting = true;

                if (_connection != null && _connection.FullState == ConnectionState.Open)
                {
                    await DisconnectAsync();
                }

                _connectionString = connectionString;
                _connection = new NpgsqlConnection(_connectionString.ToString());
                await _connection.OpenAsync();
            }
            catch (Exception ex)
            {
                _exceptionProvider?.Log(ex.Message);
            }
            finally
            {
                _commandExecuting = false;
            }
        }

        public bool Clear<Entity>() where Entity : class, new()
        {
            bool result = true;

            try
            {
                var helper = new CommandsHelper<Entity>();
                string command = helper.ClearData();
                _exceptionProvider.Log(command);

                using (var cmd = new NpgsqlCommand(command, _connection))
                {
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                _exceptionProvider.Log(ex.Message);
                result = false;
            }

            return result;
        }
        public Entity[] GetAll<Entity>() where Entity : class, new()
        {
            try
            {
                var entities = new List<Entity>();
                var helper = new CommandsHelper<Entity>();
                string command = helper.SelectData();
                _exceptionProvider.Log(command);

                using (var cmd = new NpgsqlCommand(command, _connection))
                {
                    DataTable dataTable = new DataTable();
                    dataTable.Load(cmd.ExecuteReader());

                    if (dataTable.Rows != null)
                    {
                        for (int i = 0; i < dataTable.Rows.Count; i++)
                        {
                            entities.Add(helper.CastData(dataTable, i));
                        }

                        return entities.ToArray();
                    }
                }
            }
            catch (Exception ex)
            {
                _exceptionProvider.Log(ex.Message);
            }

            return null;
        }
        public Entity GetById<Entity>(int entityId) where Entity : class, new()
        {
            try
            {
                var helper = new CommandsHelper<Entity>();
                string command = helper.SelectDataById(entityId);
                _exceptionProvider.Log(command);

                using (var cmd = new NpgsqlCommand(command, _connection))
                {
                    var dataTable = new DataTable();
                    dataTable.Load(cmd.ExecuteReader());
                    return helper.CastData(dataTable);
                }
            }
            catch (Exception ex)
            {
                _exceptionProvider.Log(ex.Message);
            }

            return null;
        }
        public int Insert<Entity>(Entity entity) where Entity : class, new()
        {
            try
            {
                object result = null;
                var helper = new CommandsHelper<Entity>(entity);
                (string command, IEnumerable<object> objects) = helper.InsertData();
                _exceptionProvider.Log(command);

                using (var cmd = new NpgsqlCommand(command, _connection))
                {
                    foreach (var value in objects)
                    {
                        cmd.Parameters.AddWithValue(value);
                    }

                    result = cmd.ExecuteScalar();
                }

                if (result is int id)
                {
                    return id;
                }
            }
            catch (Exception ex)
            {
                _exceptionProvider.Log(ex.Message);
            }

            return -1;
        }
        public bool Remove<Entity>(int entityId) where Entity : class, new()
        {
            bool result = true;

            try
            {
                var helper = new CommandsHelper<Entity>();
                string command = helper.DeleteData(entityId);
                _exceptionProvider.Log(command);

                using (var cmd = new NpgsqlCommand(command, _connection))
                {
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                _exceptionProvider.Log(ex.Message);
                result = false;
            }

            return result;
        }
        public bool Remove<Entity>(int[] ids) where Entity : class, new()
        {
            bool result = true;

            try
            {
                var helper = new CommandsHelper<Entity>();
                string command = helper.MultipleDeleteData(ids);
                _exceptionProvider.Log(command);

                using (var cmd = new NpgsqlCommand(command, _connection))
                {
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                _exceptionProvider.Log(ex.Message);
                result = false;
            }

            return result;
        }
        public bool Update<Entity>(Entity entity) where Entity : class, new()
        {
            bool result = true;

            try
            {
                var helper = new CommandsHelper<Entity>(entity);
                (string command, IEnumerable<object> objects) = helper.UpdateData();
                _exceptionProvider.Log(command);

                using (var cmd = new NpgsqlCommand(command, _connection))
                {
                    foreach (var value in objects)
                    {
                        cmd.Parameters.AddWithValue(value);
                    }
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                _exceptionProvider.Log(ex.Message);
                result = false;
            }

            return result;
        }

        ~Manager()
        {
            if (_connection != null)
            {
                _connection.Close();
                _connection.Dispose();
            }
        }
    }
}
